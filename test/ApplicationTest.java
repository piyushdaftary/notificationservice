import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.grbh.notificationService.models.Dto.Notification;
import com.grbh.notificationService.models.GcmSendMessage;
import controllers.Application;
import org.apache.http.protocol.HTTP;
import org.junit.*;

import play.api.libs.ws.WS;
import play.mvc.*;
import play.test.*;
import play.data.DynamicForm;
import play.data.validation.ValidationError;
import play.data.validation.Constraints.RequiredValidator;
import play.i18n.Lang;
import play.libs.F;
import play.libs.F.*;
import play.twirl.api.Content;

import static play.test.Helpers.*;
import static org.junit.Assert.*;


/**
*
* Simple (JUnit) tests that can call all parts of a play app.
* If you are interested in mocking a whole application, see the wiki for more details.
*
*/
public class ApplicationTest {

    @Test
    public void simpleCheck() {
        int a = 1 + 1;
        assertEquals(2, a);
    }

    @Test
    public void renderTemplate() {
        Content html = views.html.index.render("Your new application is ready.");
        assertEquals("text/html", contentType(html));
        assertTrue(contentAsString(html).contains("Your new application is ready."));
    }

    @Test
    public void checkUpdateGcmID(){
        GcmSendMessage gcmSendMessage = new GcmSendMessage();
        gcmSendMessage.pushNotificationToGCM("d6I3_zGFaDo:APA91bEB4mWf_hXllmO78P6hA0H1Siw1EtSI9Bic2GUGz3EPPK7IKv_xxGaYiZ" +
                "-EFiUJjFspvj8owAZy9zrXlOtF_qlCc5FbzubmIt_42p168eY3b", "test");
    }

    @Test
    public void checkNotification(){
        Notification notification= new Notification();
        boolean result = notification.sendNotification("piyush","1","Sunny Sunny");
        assertEquals(result,true);

        result=notification.sendNotification("piyush","1111","Sunny Sunny");
        assertEquals(result,false);
    }




}
