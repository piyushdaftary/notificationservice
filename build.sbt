name := """notificationService"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs
)

libraryDependencies += "com.ganyo" % "gcm-server" % "1.0.2"

libraryDependencies += "org.mongodb.morphia" % "morphia" % "0.107"

libraryDependencies += "org.mongodb" % "mongo-java-driver" % "1.3"

libraryDependencies += "junit" % "junit" % "4.10"



// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator
