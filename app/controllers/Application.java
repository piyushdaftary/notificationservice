package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.grbh.notificationService.models.Dao.UserDao;
import com.grbh.notificationService.models.Dto.Notification;
import com.grbh.notificationService.models.Dto.User;
import com.grbh.notificationService.models.GcmSendMessage;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.util.JSON;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import play.mvc.*;

import settings.Global;
import views.html.*;

public class Application extends Controller {

    public Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public Result gcmTest(){
        GcmSendMessage gcmSendMessage = new GcmSendMessage();

        gcmSendMessage.pushNotificationToGCM("d6I3_zGFaDo:APA91bEB4mWf_hXllmO78P6hA0H1Siw1EtSI9Bic2GUGz3EPPK7IKv_xxGaYiZBWA6N7JjcGRaKZTg4YknADsWAeT-EFiUJjFspvj8owAZy9zrXlOtF_qlCc5FbzubmIt_42p168eY3b", "test");
        return ok();
    }

    public Result updateGcmID(){
        DBCollection collection= Global.getUserCollection();
        JsonNode inputJson = request().body().asJson();
        String json = inputJson.toString();
        DBObject dbObject = (DBObject) JSON.parse(json);
        String userId = dbObject.get("userId").toString().replace("\"","");
        String appId = dbObject.get("appId").toString().replace("\"", "");
        BasicDBObject query =new BasicDBObject().append("userId", userId).append("appId",appId);
        DBObject oldEntry = collection.findOne(query);

        while(oldEntry!=null) {
            collection.remove(oldEntry);
            oldEntry = collection.findOne(query);
        }

        UserDao userDao = new UserDao();
        if(userDao.update(inputJson)){
            return ok();
        }
        return status(401);
    }

    public Result sendNotification(){
        JsonNode json = request().body().asJson();
        Notification notification = new Notification(json.get("userId").toString(),json.get("appId").toString(),json.get("message").toString());
        notification.sendNotification(json.get("userId").toString().replace("\"",""),
                json.get("appId").toString().replace("\"", ""),json.get("message").toString().replace("\"",""));
        return ok();
    }

    public Result deactivate(){
        DBCollection collection= Global.getUserCollection();
        JsonNode jsonNode = request().body().asJson();
        String json = jsonNode.toString();
        DBObject dbObject = (DBObject) JSON.parse(json);
        String userId = dbObject.get("userId").toString().replace("\"","");
        String appId = dbObject.get("appId").toString().replace("\"", "");
        BasicDBObject query =new BasicDBObject().append("userId", userId).append("appId", appId);
        DBObject oldEntry = collection.findOne(query);
        collection.update(oldEntry, new BasicDBObject("$set", new BasicDBObject("status", false)));

        return ok();
    }

    public Result activate(){
        DBCollection collection= Global.getUserCollection();
        JsonNode jsonNode = request().body().asJson();
        String json = jsonNode.toString();
        DBObject dbObject = (DBObject) JSON.parse(json);
        String userId = dbObject.get("userId").toString().replace("\"","");
        String appId = dbObject.get("appId").toString().replace("\"", "");
        BasicDBObject query =new BasicDBObject().append("userId", userId).append("appId",appId);
        DBObject oldEntry = collection.findOne(query);
        collection.update(oldEntry, new BasicDBObject("$set", new BasicDBObject("status",true)));

        return ok();
    }


}
