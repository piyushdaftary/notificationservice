package settings;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import play.Application;
import play.GlobalSettings;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

/**
 * Created by piyush on 12/2/15.
 */
public class Global extends GlobalSettings {
    private static Mongo mongo ;
    private static DB db ;
    private static DBCollection userCollection ;

    public static Mongo getMongo() {
        return mongo;
    }

    public static void setMongo(Mongo mongo) {
        Global.mongo = mongo;
    }

    public static DB getDb() {
        return db;
    }

    public static void setDb(DB db) {
        Global.db = db;
    }

    public static DBCollection getUserCollection() {
        return userCollection;
    }

    public static void setUserCollection(DBCollection userCollection) {
        Global.userCollection = userCollection;
    }

    private class ActionWrapper extends Action.Simple {
        public ActionWrapper(Action<?> action) {
            this.delegate = action;
        }

        @Override
        public F.Promise<Result> call(Http.Context ctx) throws java.lang.Throwable {
            F.Promise<Result> result = this.delegate.call(ctx);
            Http.Response response = ctx.response();
            response.setHeader("Access-Control-Allow-Origin", "*");
            return result;
        }
    }

    @Override
    public Action<?> onRequest(Http.Request request, java.lang.reflect.Method actionMethod) {
        return new ActionWrapper(super.onRequest(request, actionMethod));
    }

    @Override
    public void onStart(Application app) {

        super.beforeStart(app);
        try {
            System.setOut(new PrintStream(new File("./sysout.log")));
            System.setErr(new PrintStream(new File("./syserr.log")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        doDBConnection(app.configuration().getString("mongo.server"), app.configuration().getInt("mongo.port"),
                app.configuration().getString("mongo.db"));
    }

    public static void doDBConnection(String mongoServer, int mongoPort, String mongoDbName){
        try {
            mongo = new Mongo(mongoServer, mongoPort);
            db = mongo.getDB(mongoDbName);
            userCollection = db.getCollection("User");
        }catch (Exception e){
            e.printStackTrace();
        }
    }



}
