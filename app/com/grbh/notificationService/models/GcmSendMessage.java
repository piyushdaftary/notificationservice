package com.grbh.notificationService.models;

import com.google.android.gcm.server.InvalidRequestException;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import java.io.IOException;

/**
 * Created by piyush on 11/30/15.
 */
public class GcmSendMessage {
    /**
     * gcmRegId is the id which android app will give to server (one time)
     **/
    public boolean pushNotificationToGCM(String gcmRegId,String message){
        final String GCM_API_KEY = "AIzaSyB3abgjKGrwv-G_jbB3kEcrICXCS0Q35X0";
        final int retries = 3;
        Sender sender = new Sender(GCM_API_KEY);
        Message msg = new Message.Builder().addData("message",message).build();

        try {
//            if(account.getGcmRegId()!=null) {
                Result result = sender.send(msg, gcmRegId, retries);
                /**
                 * if you want to send to multiple then use below method
                 * send(Message message, List<String> regIds, int retries)
                 **/


                if ((result.getErrorCodeName()) == null) {
                    System.out.println("GCM Notification is sent successfully " + result.toString());
                    return true;
                }

                System.out.println("Error occurred while sending push notification :" + result.getErrorCodeName());

//            }
        } catch (InvalidRequestException e) {
            System.out.println("Invalid Request");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO Exception");
        }
        return false;

    }
}
