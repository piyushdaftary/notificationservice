package com.grbh.notificationService.models.Dao;

import com.fasterxml.jackson.databind.JsonNode;
import com.grbh.notificationService.models.Dto.User;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.util.JSON;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

/**
 * Created by piyush on 12/2/15.
 */
public class UserDao {
    public boolean update(JsonNode json){
    try{
        String dbName = new String("notificationService");
        Mongo mongo = new Mongo();
        Morphia morphia = new Morphia();
        Datastore datastore = morphia.createDatastore(mongo, dbName);

        User user = new User();
        user.setUserId(json.get("userId").toString().replace("\"",""));
        user.setAppId(json.get("appId").toString().replace("\"", ""));
        user.setGcmId(json.get("gcmId").toString().replace("\"",""));
        user.setStatus(json.get("status").asBoolean());
        System.out.print(datastore.save(user));
    }catch (Exception e){
        e.printStackTrace();
        return false;
    }
        return true;
    }
}
