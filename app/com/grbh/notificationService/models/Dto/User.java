package com.grbh.notificationService.models.Dto;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

/**
 * Created by piyush on 12/2/15.
 */

@Entity(value = "User", noClassnameStored = true)
public class User {
    @Id
    ObjectId _id;
    private String userId;
    private String appId;
    private String gcmId;
    private boolean status;

    public User() {
        this.userId = null;
        this.appId = null;
        this.gcmId = null;
        this.status = false;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getGcmId() {
        return gcmId;
    }

    public void setGcmId(String gcmId) {
        this.gcmId = gcmId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public User(ObjectId _id, String userId, String appId, String gcmId, boolean status) {

        this._id = _id;
        this.userId = userId;
        this.appId = appId;
        this.gcmId = gcmId;
        this.status = status;
    }
}
