package com.grbh.notificationService.models.Dto;

import com.google.android.gcm.server.InvalidRequestException;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import settings.Global;

import java.io.IOException;

/**
 * Created by piyush on 12/2/15.
 */

public class Notification {
    private String userId;
    private String appId;
    private String message;
    private String gcmRegId;

    public Notification(String userId, String appId, String message) {
        this.userId = userId;
        this.appId = appId;
        this.message = message;
    }
    public Notification() {
        this.userId =null;
        this.appId = null;
        this.message = null;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean sendNotification(String userId,String appId,String message){
        final String GCM_API_KEY = "AIzaSyB3abgjKGrwv-G_jbB3kEcrICXCS0Q35X0";
        final int retries = 3;
        try {
        Sender sender = new Sender(GCM_API_KEY);
        DBCollection userCollection= Global.getUserCollection();
        BasicDBObject query =new BasicDBObject().append("userId", userId).append("appId",appId);
        DBObject userObject = userCollection.findOne(query);
//        System.out.println("gcmId" + userObject.get("userId").toString());

        this.gcmRegId = userObject.get("gcmId").toString();

        Message msg = new Message.Builder().addData("message",message).build();
//            if(account.getGcmRegId()!=null) {
            Result result = sender.send(msg, gcmRegId, retries);
            /**
             * if you want to send to multiple then use below method
             * send(Message message, List<String> regIds, int retries)
             **/

            if ((result.getErrorCodeName()) == null) {
                System.out.println("GCM Notification is sent successfully " + result.toString());
                return true;
            }

            System.out.println("Error occurred while sending push notification :" + result.getErrorCodeName());

//            }
        }catch (Exception e) {
            System.out.println("Exception");
            e.printStackTrace();
        }
        return false;

    }
}
